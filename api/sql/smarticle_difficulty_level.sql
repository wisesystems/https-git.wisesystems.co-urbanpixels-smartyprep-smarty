create sequence smarticle_difficulty_level_id_seq;

CREATE TABLE smarticle_difficulty_level
(
    id int DEFAULT nextval('smarticle_difficulty_level_id_seq'::regclass) PRIMARY KEY NOT NULL,
    difficulty_level int NOT NULL,
    smarticle_id int NOT NULL,
    standard_level boolean NOT NULL,
);
CREATE UNIQUE INDEX smarticle_difficulty_level_id_uindex ON smarticle_difficulty_level (id);