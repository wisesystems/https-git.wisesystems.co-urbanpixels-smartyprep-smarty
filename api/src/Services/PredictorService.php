<?php

namespace App\Services;


use App\Entity\Question;
use App\Entity\QuestionDifficulty;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Symfony\Component\Config\Definition\Exception\Exception;

class PredictorService
{
    const TYPE_SKILLDRILL_MATH = 'MathSkillDrill';

    protected $em;
    private $userId;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function predictQuestion($skillDrill) {
        if($skillDrill["type"] == self::TYPE_SKILLDRILL_MATH) {
            //If the user doesn't have any previous skill_drill
            // we get la last question from the category
            if(!$skillDrill['current_question_id']) {
                $question = $this->getMathUnansweredUserQuestion(
                    $skillDrill['question_category_subgroup_id'],
                    QuestionDifficulty::LEVEL_STANDARD
                );
            }else{
                //Get Difficulty level based on AnsweredQuestions from same category
                $question = $this->getMathQuestion($skillDrill['question_category_subgroup_id']);
            }
            //Update the Skill Drill and call RUBY
            if($this->updateSkillDrillQuestion($skillDrill["id"], $question)) {
                return $question;
            };
        }
        return false;
    }

    /**
     * Get Math question from same Subgroup
     * based on difficulty level for the previous answers
     *
     * @param int $subgroupId
     * @return array|null
     * @throws DBALException
     */
    private function getMathQuestion($subgroupId) {
        $question = null;
        $questionsDifficulty = $this->getMathDifficultyLevelByGroup($subgroupId);
        $levels = array();
        foreach ($questionsDifficulty as $questionDifficulty) {
            $levels[] = $questionDifficulty["difficulty_level"];
        }
        if($levels) {
            $levels = array_count_values($levels);
            arsort($levels);
            $difficultyLevel = key($levels);
            if($difficultyLevel) {
                //Get question with same difficulty level
                $connection = $this->em->getConnection();
                $statement = $connection->prepare("
                    SELECT DISTINCT q.id,q.created_at, qdl.difficulty_level
                        FROM questions q
                          INNER JOIN question_difficulty_level qdl ON qdl.question_id = q.id
                          INNER JOIN user_answers ua ON q.id = ua.question_id
                          INNER JOIN question_categories_questions qcq ON qcq.question_id = q.id
                          INNER JOIN question_categories qc ON qc.id = qcq.question_category_id
                                                               AND qc.type IN ('MathQuestionCategory')
                        
                        WHERE q.type IN ('MathQuestion')
                              AND q.is_draft = FALSE
                              AND qc.math_category_subgroup_id  =:subgroupId
                              AND ua.user_id != :userId
                              AND (qdl.difficulty_level = :level AND q.purpose = 'Practice' )
                        ORDER BY q.created_at DESC
                        LIMIT 1
                ");
                $statement->bindValue('userId', $this->getUserId());
                $statement->bindValue('subgroupId', $subgroupId);
                $statement->bindValue('level', $difficultyLevel);
                $statement->execute();
                $question = $statement->fetch();
            }
        }
        return $question;
    }

    private function getMathDifficultyLevelByGroup($subgroupId) {
        $connection = $this->em->getConnection();
        $statement = $connection->prepare("
            SELECT  qdl.difficulty_level
              FROM question_difficulty_level qdl
              INNER JOIN questions q ON qdl.question_id = q.id
              INNER JOIN question_categories_questions qcq ON qcq.question_id = q.id
              INNER JOIN question_categories qc ON qc.id = qcq.question_category_id
                    AND qc.type IN ('MathQuestionCategory')
              INNER JOIN user_answers ua ON q.id = ua.question_id
              WHERE qc.math_category_subgroup_id = :subgroupId
              AND ua.user_id = :userId
          "
        );
        $statement->bindValue('userId', $this->getUserId());
        $statement->bindValue('subgroupId', $subgroupId);
        $statement->execute();
        return $statement->fetchAll();
    }

    private function updateSkillDrillQuestion($id, $question) {
        if($question) {
            $connection = $this->em->getConnection();
            try {
                $connection->executeUpdate(
                    'UPDATE skill_drills SET current_question_id = ? WHERE id = ?',
                        array($question['id'], $id));
                return true;
            }catch (DBALException $e) {
                echo \GuzzleHttp\json_encode($e->getMessage());
                exit;
            }
        }
    }

    /**
     * Select Active & Unanswered Question for an User and
     * for a Specific Category from Math Group using skillDrill
     *
     * @param int $categoryId
     * @param int $difficultyLevel
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getMathUnansweredUserQuestion($categoryId, $difficultyLevel) {
        $connection = $this->em->getConnection();
        $statement = $connection->prepare("
              SELECT  q.id, q.title, q.user_id, qdl.difficulty_level
              FROM questions q
              INNER JOIN question_categories_questions qcq ON qcq.question_id = q.id
              INNER JOIN question_categories qc ON qc.id = qcq.question_category_id
                    AND qc.type IN ('MathQuestionCategory')
              LEFT OUTER JOIN user_answers ua ON q.id = ua.question_id
                    AND ua.user_id = :userId
              LEFT JOIN question_difficulty_level qdl ON qdl.question_id = q.id
              WHERE q.type IN ('MathQuestion')
              AND q.is_draft = FALSE
              AND qc.math_category_subgroup_id = :categoryId
              AND ua.user_id IS NULL
              AND (qdl.difficulty_level = :level AND q.purpose = 'Practice' )
              ORDER BY RANDOM()
              LIMIT 1
        ");
        $statement->bindValue('userId', $this->getUserId());
        $statement->bindValue('categoryId', $categoryId);
        $statement->bindValue('level', $difficultyLevel);
        $statement->execute();
        return $statement->fetch();
    }

    /**
     * @return int
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId) {
        $this->userId = $userId;
    }
}