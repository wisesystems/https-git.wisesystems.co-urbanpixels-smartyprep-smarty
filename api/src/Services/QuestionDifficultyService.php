<?php
namespace App\Services;

use App\Entity\Question;
use App\Entity\QuestionDifficulty;
use App\Entity\QuestionSet;
use App\Entity\Smarticle;
use App\Entity\SmarticleDifficulty;
use App\Entity\UserAnswer;
use App\Repository\QuestionDifficultyRepository;
use App\Repository\QuestionRepository;
use App\Repository\SmarticleRepository;
use Doctrine\ORM\EntityManagerInterface;

class QuestionDifficultyService
{
    protected $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getAllQuestions() {
        /** @var QuestionRepository $repository */
        $repository = $this->em->getRepository("App:Question");
        return $repository->findAllActive();
    }

    public function getSmarticlesQuestionDifficulty() {
        /** @var SmarticleRepository $repository */
        $repository = $this->em->getRepository("App:Smarticle");
        return $repository->findQuestionDifficultySmarticles();
    }

    public function createQuestionDifficulty(Question $question) {
        $difficultyLevel = $this->em
            ->getRepository("App:QuestionDifficulty")
            ->find($question->getId());

        if(!$difficultyLevel) {
            $difficultyLevel = new QuestionDifficulty();
            $difficultyLevel->setQuestion($question);
        }
        $difficultyLevel->setDifficultyLevel(QuestionDifficulty::LEVEL_STANDARD);
        $difficultyLevel->setStandardLevel(QuestionDifficulty::IS_STANDARD_LVL);

        if($question->getUserAnswers()->count() >= QuestionDifficulty::MIN_ANSWERS) {
            $question->getAnswersStats();

            $incorrectAnswerPercentage = $question->getIncorrectPercentage();
            $difficultyLevel->setDifficultyLevel( $difficultyLevel->getLevelByPercentage($incorrectAnswerPercentage));
            $difficultyLevel->setStandardLevel(QuestionDifficulty::NOT_STANDARD_LVL);
        }
        return $difficultyLevel;
    }

    public function setSmarticleDifficulty(Smarticle $smarticle) {
        /** @var SmarticleDifficulty $smarticleDifficulty */
        $smarticleDifficulty = $smarticle->getSmarticleDifficulty();
        if(!$smarticleDifficulty) {
            $smarticleDifficulty = new SmarticleDifficulty();
            $smarticleDifficulty->setSmarticle($smarticle);
        }

        $levels = array();
        /** @var Question $question */
        foreach ($smarticle->getQuestions() as $question) {
            $levels[] = $question->getQuestionDifficulty()->getDifficultyLevel();
        }
        $levels = array_count_values($levels);
        arsort($levels);
        $difficultyLevel = key($levels);

        $smarticleDifficulty->setDifficultyLevel($difficultyLevel);

        return $smarticleDifficulty;
    }
}