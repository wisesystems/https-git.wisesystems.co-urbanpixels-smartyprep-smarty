<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\QuestionSet;

/**
 * @ORM\Table(name="smarticles")
 * @ORM\Entity(repositoryClass="App\Repository\SmarticleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Smarticle
{

    const IS_DRAFT = true;
    const IS_NOT_DRAFT = false;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDraft;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var QuestionSet
     * @ORM\OneToOne(targetEntity="QuestionSet", mappedBy="smarticle")
     */
    private $questionSet;

    /**
     * @ORM\OneToOne(targetEntity="SmarticleDifficulty", mappedBy="smarticle")
     */
    private $smarticleDifficulty;


    public function __construct() {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIsDraft()
    {
        return $this->isDraft;
    }

    /**
     * @param mixed $isDraft
     */
    public function setIsDraft($isDraft)
    {
        $this->isDraft = $isDraft;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getQuestionSet()
    {
        return $this->questionSet;
    }

    /**
     * @param mixed $questionSet
     */
    public function setQuestionSet($questionSet)
    {
        $this->questionSet = $questionSet;
    }

    /**
     * @return mixed
     */
    public function getSmarticleDifficulty()
    {
        return $this->smarticleDifficulty;
    }

    /**
     * @param mixed $smarticleDifficulty
     */
    public function setSmarticleDifficulty($smarticleDifficulty)
    {
        $this->smarticleDifficulty = $smarticleDifficulty;
    }

    /**
     * @return mixed
     */
    public function getQuestions()
    {
        if($this->getQuestionSet()) {
            return $this->getQuestionSet()->getQuestions();
        }
        return null;
    }
}