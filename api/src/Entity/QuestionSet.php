<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Smarticle;
use App\Entity\Question;

/**
 * @ORM\Table(name="question_sets")
 * @ORM\Entity(repositoryClass="App\Repository\QuestionSetRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class QuestionSet
{

    /**
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Smarticle", inversedBy="questionSet")
     * @ORM\JoinColumn(name="smarticle_id", referencedColumnName="id")
     */
    private $smarticle;

    /**
     * @ORM\OneToMany(targetEntity="Question", mappedBy="questionSet")
     */
    private $questions;

    public function __construct() {
        $this->questions = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSmarticle()
    {
        return $this->smarticle;
    }

    /**
     * @param mixed $smarticle
     */
    public function setSmarticle($smarticle)
    {
        $this->smarticle = $smarticle;
    }

    /**
     * @return mixed
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param mixed $questions
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;
    }

    public function addQuestion(Question $question) {
        $this->questions->add($question);
        return $this;
    }

    public function removeQuestion(Question $question) {
        $this->questions->removeElement($question);
        return $this;
    }
}