<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="smarticle_difficulty_level")
 * @ApiResource
 * @ORM\Entity
 */
class SmarticleDifficulty
{
    /**
     * @var int The entity Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    public $difficultyLevel;


    /**
     * @ORM\OneToOne(targetEntity="Smarticle", inversedBy="smarticleDifficulty")
     * @ORM\JoinColumn(name="smarticle_id", referencedColumnName="id")
     */
    private $smarticle;

    public function __construct()
    {

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDifficultyLevel()
    {
        return $this->difficultyLevel;
    }

    /**
     * @param mixed $difficultyLevel
     */
    public function setDifficultyLevel($difficultyLevel)
    {
        $this->difficultyLevel = $difficultyLevel;
    }

    /**
     * @return mixed
     */
    public function getSmarticle()
    {
        return $this->smarticle;
    }

    /**
     * @param mixed $smarticle
     */
    public function setSmarticle($smarticle)
    {
        $this->smarticle = $smarticle;
    }
}
