<?php

namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\QuestionSet;

/**
 * @ORM\Table(name="questions")
 * @ORM\Entity(repositoryClass="App\Repository\QuestionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Question
{
    const TYPE_MATH = 'MathQuestion';

    /**
     * @var int The entity Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="QuestionSet", inversedBy="questions")
     * @ORM\JoinColumn(name="question_set_id", referencedColumnName="id")
     */
    private $questionSet;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="UserAnswer", mappedBy="question")
     */
    private $userAnswers;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDraft;

    /**
     * @ORM\OneToOne(targetEntity="QuestionDifficulty", mappedBy="question")
     */
    private $questionDifficulty;

    private $totalCorrectAnswers = 0;
    private $totalIncorrectAnswers = 0;
    private $totalUnknownAnswers = 0;

    public function __construct()
    {
        $this->userAnswers = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return QuestionSet | null
     */
    public function getQuestionSet()
    {
        return $this->questionSet;
    }

    /**
     * @param mixed $questionSet
     */
    public function setQuestionSet($questionSet)
    {
        $this->questionSet = $questionSet;
    }

    /**
     * @return ArrayCollection
     */
    public function getUserAnswers()
    {
        return $this->userAnswers;
    }

    /**
     * @param ArrayCollection $userAnswers
     */
    public function setUserAnswers($userAnswers)
    {
        $this->userAnswers = $userAnswers;
    }

    public function addUserAnswer(UserAnswer $userAnswer) {
        $this->userAnswers->add($userAnswer);
        return $this;
    }

    public function removeUserAnswers(UserAnswer $userAnswer) {
        $this->userAnswers->removeElement($userAnswer);
    }

    /**
     * @return int
     */
    public function getTotalCorrectAnswers()
    {
        return $this->totalCorrectAnswers;
    }

    /**
     * @param int $totalCorrectAnswers
     */
    public function setTotalCorrectAnswers($totalCorrectAnswers)
    {
        $this->totalCorrectAnswers = $totalCorrectAnswers;
    }

    /**
     * @return int
     */
    public function getTotalIncorrectAnswers()
    {
        return $this->totalIncorrectAnswers;
    }

    /**
     * @param int $totalIncorrectAnswers
     */
    public function setTotalIncorrectAnswers($totalIncorrectAnswers)
    {
        $this->totalIncorrectAnswers = $totalIncorrectAnswers;
    }

    /**
     * @return int
     */
    public function getTotalUnknownAnswers()
    {
        return $this->totalUnknownAnswers;
    }

    /**
     * @param int $totalUnknownAnswers
     */
    public function setTotalUnknownAnswers($totalUnknownAnswers)
    {
        $this->totalUnknownAnswers = $totalUnknownAnswers;
    }

    /**
     * @return mixed
     */
    public function getQuestionDifficulty()
    {
        return $this->questionDifficulty;
    }

    /**
     * @param mixed $questionDifficulty
     */
    public function setQuestionDifficulty($questionDifficulty)
    {
        $this->questionDifficulty = $questionDifficulty;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getIsDraft()
    {
        return $this->isDraft;
    }

    /**
     * @param mixed $isDraft
     */
    public function setIsDraft($isDraft)
    {
        $this->isDraft = $isDraft;
    }

    public function isMathQuestion() {
        return $this->type === self::TYPE_MATH ? true : false;
    }

    public function getAnswersStats() {

        /** @var UserAnswer $userAnswer */
        foreach ($this->getUserAnswers() as $userAnswer) {
            if($userAnswer->getIsCorrect() === UserAnswer::IS_CORRECT) {
                $this->totalCorrectAnswers++;
            }elseif ($userAnswer->getIsCorrect() === UserAnswer::IS_INCORRECT) {
                $this->totalIncorrectAnswers++;
            }else{
                $this->totalUnknownAnswers++;
            }
        }
    }

    public function getIncorrectPercentage() {
        $totalToCalc = count($this->userAnswers) - $this->totalUnknownAnswers;
        if($totalToCalc > 0) {
            return number_format($this->totalIncorrectAnswers *100 / $totalToCalc, 2);
        }
        return 0;
    }
}