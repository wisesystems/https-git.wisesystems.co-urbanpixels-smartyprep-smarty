<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="question_difficulty_level")
 * @ApiResource
 * @ORM\Entity(repositoryClass="App\Repository\QuestionDifficultyRepository")
 */
class QuestionDifficulty
{
    const MIN_ANSWERS = 3;

    const LEVEL_VERY_EASY = 1;
    const LEVEL_EASY = 2;
    const LEVEL_MEDIUM = 3;
    const LEVEL_HARD = 4;
    const LEVEL_VERY_HARD = 5;
    const LEVEL_STANDARD = self::LEVEL_MEDIUM;

    const IS_STANDARD_LVL = true;
    const NOT_STANDARD_LVL = false;

    private $difficultyRange = array(
        self::LEVEL_VERY_EASY => array(0,25),
        self::LEVEL_EASY => array(25,50),
        self::LEVEL_MEDIUM => array(50,75),
        self::LEVEL_HARD => array(75,90),
        self::LEVEL_VERY_HARD => array(90,100),
    );

    /**
     * @var int The entity Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    public $difficultyLevel;


    /**
     * @ORM\OneToOne(targetEntity="Question", inversedBy="questionDifficulty")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $standardLevel;

    public function __construct()
    {

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDifficultyLevel()
    {
        return $this->difficultyLevel;
    }

    /**
     * @param mixed $difficultyLevel
     */
    public function setDifficultyLevel($difficultyLevel)
    {
        $this->difficultyLevel = $difficultyLevel;
    }

    /**
     * @return Question | null
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param mixed $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return bool
     */
    public function isStandardLevel()
    {
        return $this->standardLevel;
    }

    /**
     * @param bool $standardLevel
     */
    public function setStandardLevel($standardLevel)
    {
        $this->standardLevel = $standardLevel;
    }

    public function getLevelByPercentage($incorrectPercentage) {
        switch ($incorrectPercentage) {
            case 0:
                return self::LEVEL_VERY_EASY;
            case 100;
                return self::LEVEL_VERY_HARD;
            default:
                foreach ($this->difficultyRange as $level => $range) {
                    if($incorrectPercentage > $range[0] && $incorrectPercentage <= $range[1]) {
                        return $level;
                    }
                }
        }
        return self::LEVEL_STANDARD;
    }
}
