<?php

namespace App\Repository;

use App\Entity\Question;
use App\Entity\Smarticle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Question|null find($id, $lockMode = null, $lockVersion = null)
 * @method Question|null findOneBy(array $criteria, array $orderBy = null)
 * @method Question[]    findAll()
 * @method Question[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Question::class);
    }

    /**
     * The MathQuestion type use is_draft field !!!
     * @return mixed
     *
     *      SELECT
     *        q.id, q.type, q.is_draft,
     *             qs.id as question_set_id,
     *             s.id as smarticle_id,
     *             s.is_draft as smarticle_draft
     *         FROM questions q
     *             LEFT JOIN question_sets qs ON q.question_set_id=qs.id
     *             LEFT JOIN smarticles s ON s.id=qs.smarticle_id
     *
     *          WHERE
     *             (q.type is null AND s.is_draft = false)  OR
     *             (q.type='MathQuestion' and q.is_draft = false)
     *
     */
    public function findAllActive()
    {
        $qb = $this->createQueryBuilder('q')
            ->leftJoin('q.questionSet', 'qs')
            ->leftJoin('qs.smarticle', 's');

        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->isNull('q.type'),
                $qb->expr()->eq('s.isDraft', ':sDraft')
            )
        );
        $qb->orWhere(
            $qb->expr()->andX(
                $qb->expr()->eq('q.type', ':qType'),
                $qb->expr()->eq('q.isDraft', ':qDraft')
            )
        )
        ->setParameter('qType', Question::TYPE_MATH)
        ->setParameter('qDraft', Smarticle::IS_NOT_DRAFT)
        ->setParameter('sDraft', Smarticle::IS_NOT_DRAFT)
        ;

        return $qb->getQuery()->getResult();
    }
}
