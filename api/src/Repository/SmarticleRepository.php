<?php

namespace App\Repository;

use App\Entity\Smarticle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Smarticle|null find($id, $lockMode = null, $lockVersion = null)
 * @method Smarticle|null findOneBy(array $criteria, array $orderBy = null)
 * @method Smarticle[]    findAll()
 * @method Smarticle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SmarticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Smarticle::class);
    }

    public function findQuestionDifficultySmarticles() {

        return $this->createQueryBuilder('s')
            ->addSelect('s.id, s.isDraft,s.title')
            ->addSelect('qdl.id as difficulty_id, qdl.difficultyLevel')

            ->innerJoin('s.questionSet', 'qs')
            ->innerJoin('qs.questions', 'q')
            ->innerJoin('q.questionDifficulty', 'qdl')
            ->andWhere('s.isDraft = :val')
            ->setParameter('val', Smarticle::IS_NOT_DRAFT)
            ->addGroupBy('qdl.id')
            ->addGroupBy('s.id')
            ->getQuery()
            ->getResult()
            ;
    }
}
