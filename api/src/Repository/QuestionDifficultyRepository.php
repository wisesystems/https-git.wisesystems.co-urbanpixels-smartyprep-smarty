<?php

namespace App\Repository;

use App\Entity\QuestionDifficulty;
use App\Entity\Smarticle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuestionDifficulty|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionDifficulty|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionDifficulty[]    findAll()
 * @method QuestionDifficulty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionDifficultyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, QuestionDifficulty::class);
    }


}
