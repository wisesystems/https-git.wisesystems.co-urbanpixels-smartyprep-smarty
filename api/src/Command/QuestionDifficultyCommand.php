<?php
namespace App\Command;

use App\Entity\Question;
use App\Entity\QuestionDifficulty;
use App\Entity\Smarticle;
use App\Entity\SmarticleDifficulty;
use App\Services\QuestionDifficultyService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class QuestionDifficultyCommand extends Command
{
    private $questionDifficultyService;
    protected $em;

    public function __construct(
        QuestionDifficultyService $questionDifficultyService,
        EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        $this->questionDifficultyService = $questionDifficultyService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:difficulty-question')
            ->setDescription('Set the difficulty for all questions based on the users answers')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['Set Difficulty questions & smarticles', '============', '',]);

        $questions = $this->questionDifficultyService->getAllQuestions();
        if($questions) {
            $i = 0;
            $progressBar = new ProgressBar($output, count($questions));
            $progressBar->start();

            /** @var Question $question */
            foreach ($questions as $question) {
                /** @var QuestionDifficulty $questionDifficulty */
                $questionDifficulty = $this->questionDifficultyService->createQuestionDifficulty($question);
                $this->em->persist($questionDifficulty);

                if($i%100 == 0 || count($questions) == $i+1) {
                    $this->em->flush();
                }
                $progressBar->advance();
                $i++;
            }
            $progressBar->finish();

            $smarticles = $this->questionDifficultyService->getSmarticlesQuestionDifficulty();
            if($smarticles) {
                $i = 0;
                $progressBar = new ProgressBar($output, count($smarticles));
                $progressBar->start();
                foreach ($smarticles as  $s) {
                    /** @var Smarticle $smarticle */
                    $smarticle = $s[0];
                    $smarticleDifficulty = $this->questionDifficultyService->setSmarticleDifficulty($smarticle);
                    $this->em->persist($smarticleDifficulty);

                    if($i%100 == 0 || count($questions) == $i+1) {
                        $this->em->flush();
                    }
                    $progressBar->advance();
                    $i++;
                }
                $progressBar->finish();
            }

            $output->writeln('');
            $output->writeln('Total Smarticles :'.count($smarticles));
        }
    }
}