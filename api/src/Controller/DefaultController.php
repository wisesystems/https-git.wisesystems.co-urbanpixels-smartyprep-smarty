<?php

namespace App\Controller;

use App\Services\PredictorService;
use App\Services\QuestionDifficultyService;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends AbstractController
{
    /** @var QuestionDifficultyService */
    private $predictorDifficultyService;

    /** @var PredictorService */
    private $predictorService;

    public function __construct(
        QuestionDifficultyService $predictorDifficultyService,
        PredictorService $predictorService
    )
    {
        $this->predictorDifficultyService = $predictorDifficultyService;
        $this->predictorService = $predictorService;
    }

    /**
     * @Route("/skill-drills/{type}/{skillDrillId}", name="skill-drills")
     * @return JsonResponse
     */
    public function index(Request $request, $type, $skillDrillId)
    {
        $skillDrill = $this->getSkillDrill($skillDrillId);
        if($skillDrill) {
            $currentUser = $this->getCurrentUser($skillDrill["user_id"]);
            if($currentUser) {
                $this->predictorService->setUserId($skillDrill["user_id"]);

                $predictedQuestion = $this->predictorService->predictQuestion($skillDrill);
                if($predictedQuestion){
                    return new JsonResponse($predictedQuestion['id'], 200);
                };
            }
        }
        return new JsonResponse(null, 400);
    }

    private function getCurrentUser($id) {
        if($id) {
            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("SELECT id,email,last_name FROM users WHERE id = :id");
            $statement->bindValue('id', $id);
            $statement->execute();
            return $statement->fetch();
        }
        return null;
    }

    private function getSkillDrill($id) {
        if($id) {
            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("SELECT * FROM skill_drills s WHERE s.id = :id");
            $statement->bindValue('id', $id);
            $statement->execute();
            return $statement->fetch();
        }
        return null;
    }
}