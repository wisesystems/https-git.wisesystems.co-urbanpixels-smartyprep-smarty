- Allow access to all databases for all users with an encrypted password:
1. postgresql.conf: `listen_addresses = '*'`
2. pg_hba.conf: <br>
`# TYPE DATABASE USER CIDR-ADDRESS  METHOD`<br> 
 `host  all  all 0.0.0.0/0 md5`
 3. restart `/etc/init.d/postgresql restart`